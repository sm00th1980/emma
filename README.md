# README

### Install dependenices

```
> yarn install
```

### Linter

```
> yarn lint
```

### Tests

```
> yarn test
> yarn test:watch
```

### Format

```
> yarn format
```

### Check typings

```
> yarn types
```
