import {ImageSourcePropType} from 'react-native';
import {times} from 'ramda';

import AlanMunger from '../assets/allan_munger.png';
import AmandaBrady from '../assets/amanda_brady.png';
import AshleyMcCarthy from '../assets/ashley_mc_carthy.png';
import CarlosSlattery from '../assets/carlos_slattery.png';
import CarolePoland from '../assets/carole_poland.png';
import CecilFolk from '../assets/cecil_folk.png';
import CelesteBurton from '../assets/celeste_burton.png';
import CharlotteWaltson from '../assets/charlotte_waltson.png';
import ColinBallinger from '../assets/colin_ballinger.png';
import DaisyPhillips from '../assets/daisy_phillips.png';
import ElliotWoodward from '../assets/elliot_woodward.png';
import ElviaAtkins from '../assets/elvia_atkins.png';
import ErikNason from '../assets/erik_nason.png';
import HenryBrill from '../assets/henry_brill.png';
import IsaacFielder from '../assets/isaac_fielder.png';
import JohnyMcconnell from '../assets/johnie_mcconnell.png';
import KatLarsson from '../assets/kat_larsson.png';
import KatriAhokas from '../assets/katri_ahokas.png';
import KevinSturgis from '../assets/kevin_sturgis.png';
import KristinPatterson from '../assets/kristin_patterson.png';
import LydiaBauer from '../assets/lydia_bauer.png';
import MauricioAugust from '../assets/mauricio_august.png';
import MiguelGarcia from '../assets/miguel_garcia.png';
import MonaKane from '../assets/mona_kane.png';
import RobertTolbert from '../assets/robert_tolbert.png';
import RobinCounts from '../assets/robin_counts.png';
import TimDeboer from '../assets/tim_deboer.png';
import WandaHoward from '../assets/wanda_howard.png';

const People = [
  {
    image: AlanMunger,
    name: 'Alan Munger',
    title: 'title 1',
  },
  {
    image: AmandaBrady,
    name: 'Amanda Brady',
    title: 'title 2',
  },
  {
    image: AshleyMcCarthy,
    name: 'Ashley Mc Carthy',
    title: 'title 3',
  },
  {
    image: CarlosSlattery,
    name: 'Carlos Slattery',
    title: 'title 4',
  },
  {
    image: CarolePoland,
    name: 'Carole Poland',
    title: 'title 5',
  },
  {
    image: CecilFolk,
    name: 'Cecil Folk',
    title: 'title 6',
  },
  {
    image: CelesteBurton,
    name: 'Celeste Burton',
    title: 'title 7',
  },
  {
    image: CharlotteWaltson,
    name: 'Charlotte Waltson',
    title: 'title 8',
  },
  {
    image: ColinBallinger,
    name: 'Colin Ballinger',
    title: 'title 9',
  },
  {
    image: DaisyPhillips,
    name: 'Daisy Phillips',
    title: 'title 10',
  },
  {
    image: ElliotWoodward,
    name: 'Elliot Woodward',
    title: 'title 11',
  },
  {
    image: ElviaAtkins,
    name: 'Elvia Atkins',
    title: 'title 12',
  },
  {
    image: ErikNason,
    name: 'Erik Nason',
    title: 'title 13',
  },
  {
    image: HenryBrill,
    name: 'Henry Brill',
    title: 'title 14',
  },
  {
    image: IsaacFielder,
    name: 'Isaac Fielder',
    title: 'title 15',
  },
  {
    image: JohnyMcconnell,
    name: 'Johny Mcconnell',
    title: 'title 16',
  },
  {
    image: KatLarsson,
    name: 'Kat Larsson',
    title: 'title 17',
  },
  {
    image: KatriAhokas,
    name: 'Katri Ahokas',
    title: 'title 18',
  },
  {
    image: KevinSturgis,
    name: 'Kevin Sturgis',
    title: 'title 19',
  },
  {
    image: KristinPatterson,
    name: 'Kristin Patterson',
    title: 'title 20',
  },
  {
    image: LydiaBauer,
    name: 'Lydia Bauer',
    title: 'title 21',
  },
  {
    image: MauricioAugust,
    name: 'Mauricio August',
    title: 'title 22',
  },
  {
    image: MiguelGarcia,
    name: 'Miguel Garcia',
    title: 'title 23',
  },
  {
    image: MonaKane,
    name: 'Mona Kane',
    title: 'title 24',
  },
  {
    image: RobertTolbert,
    name: 'Robert Tolbert',
    title: 'title 25',
  },
  {
    image: RobinCounts,
    name: 'Robin Counts',
    title: 'title 26',
  },
  {
    image: TimDeboer,
    name: 'Tim Deboer',
    title: 'title 27',
  },
  {
    image: WandaHoward,
    name: 'Wanda Howard',
    title: 'title 28',
  },
];

export type Person = {
  id: string;
  isFirst: boolean;
  isLast: boolean;
  src: ImageSourcePropType;
  name: string;
  title: string;
};

export const Persons: Person[] = times((index: number) => {
  const item = {
    id: `${index}`,
    isFirst: false,
    isLast: false,
    src: People[index].image,
    name: People[index].name,
    title: People[index].title,
  };

  if (index === 0) {
    return {...item, isFirst: true};
  }

  if (index + 1 >= People.length) {
    return {...item, isLast: true};
  }

  return item;
}, People.length);
