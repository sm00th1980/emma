import React from 'react';
import {View, StyleSheet, Text} from 'react-native';

import {CLR_BLACK, CLR_GREY_09} from '../../../../../constants/colors';

const styles = (height: number, width: number) =>
  StyleSheet.create({
    container: {
      width,
      height,
    },
    headerContainer: {
      height: 80,
      justifyContent: 'center',
      alignItems: 'center',
    },
    name: {
      fontSize: 18,
      color: CLR_BLACK,
    },
    title: {
      fontSize: 18,
      color: CLR_BLACK,
    },
    descriptionContainer: {
      margin: 15,
      flex: 1,
    },
    descriptionTitle: {
      fontSize: 20,
      color: CLR_BLACK,
      marginBottom: 5,
    },
    descriptionText: {
      fontSize: 16,
      color: CLR_GREY_09,
    },
  });

type Props = {
  title: string;
  name: string;
  height: number;
  width: number;
};

const Item = ({title, name, height, width}: Props) => (
  <View style={styles(height, width).container}>
    <View style={styles(height, width).headerContainer}>
      <Text style={styles(height, width).name}>{name}</Text>
      <Text style={styles(height, width).title}>{title}</Text>
    </View>
    <View style={styles(height, width).descriptionContainer}>
      <Text style={styles(height, width).descriptionTitle}>About me</Text>
      <Text style={styles(height, width).descriptionText}>
        This is a long long description
      </Text>
    </View>
  </View>
);

export default Item;
