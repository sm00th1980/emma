import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';

import {CLR_GREY_17} from './constants/colors';
import People from './ui/screens/People';

const styles = StyleSheet.create({
  container: {
    backgroundColor: CLR_GREY_17,
    flex: 1,
  },
});

const App = () => (
  <SafeAreaView style={styles.container}>
    <People />
  </SafeAreaView>
);

export default App;
