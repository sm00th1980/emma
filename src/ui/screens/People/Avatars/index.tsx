import React, {RefObject} from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  NativeSyntheticEvent,
  NativeScrollEvent,
} from 'react-native';

import Avatar from './Avatar';
import {Person} from '../../../../constants/data';
import {getIndexByOffsetX} from './utils';
import {CLR_WHITE} from '../../../../constants/colors';

export const AVATAR_WIDTH = 64;
export const SEPARATOR_WIDTH = 20;

const styles = StyleSheet.create({
  container: {
    height: 100,
  },
  list: {
    backgroundColor: CLR_WHITE,
  },
});

type Props = {
  activeAvatarIndex: number;
  onChangeActiveAvatarIndex: (index: number) => void;
  onAvatarClick: (index: number) => void;
  innerRef: RefObject<FlatList>;
  people: Person[];
};

const Avatars = ({
  activeAvatarIndex,
  onChangeActiveAvatarIndex,
  onAvatarClick,
  innerRef,
  people,
}: Props) => {
  const handleScrollEnd = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    const positionX = event.nativeEvent.contentOffset.x;
    const index = getIndexByOffsetX(
      positionX,
      people.length,
      AVATAR_WIDTH,
      SEPARATOR_WIDTH,
    );

    onChangeActiveAvatarIndex(index);
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={people}
        renderItem={({item, index}) => (
          <Avatar
            isFirst={item.isFirst}
            isLast={item.isLast}
            avatarWidth={AVATAR_WIDTH}
            separatorWidth={SEPARATOR_WIDTH}
            isActive={index === activeAvatarIndex}
            src={item.src}
            index={index}
            onClick={onAvatarClick}
          />
        )}
        keyExtractor={item => item.id}
        horizontal
        style={styles.list}
        showsHorizontalScrollIndicator={false}
        snapToInterval={AVATAR_WIDTH + SEPARATOR_WIDTH}
        onMomentumScrollEnd={handleScrollEnd}
        ref={innerRef}
      />
    </View>
  );
};

export default Avatars;
