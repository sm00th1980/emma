import React, {useState, RefObject} from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  NativeSyntheticEvent,
  NativeScrollEvent,
  LayoutChangeEvent,
} from 'react-native';

import Item from './Item';
import {Person} from '../../../../constants/data';
import {CLR_WHITE} from '../../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: CLR_WHITE,
  },
});

type Props = {
  people: Person[];
  onScrollToPage: (index: number) => void;
  innerRef: RefObject<FlatList>;
};

const Content = ({people, onScrollToPage, innerRef}: Props) => {
  const [size, setSize] = useState({height: 0, width: 0});

  const handleScrollEnd = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    const {contentOffset, layoutMeasurement: viewSize} = event.nativeEvent;

    const pageIndex = Math.floor(contentOffset.y / viewSize.height);
    onScrollToPage(pageIndex);
  };

  return (
    <View style={styles.container}>
      <FlatList
        onLayout={(event: LayoutChangeEvent) => {
          setSize({
            height: event.nativeEvent.layout.height,
            width: event.nativeEvent.layout.width,
          });
        }}
        data={people}
        renderItem={({item}: {item: Person}) => (
          <Item
            title={item.title}
            name={item.name}
            height={size.height}
            width={size.width}
          />
        )}
        keyExtractor={item => item.id}
        pagingEnabled
        onMomentumScrollEnd={handleScrollEnd}
        ref={innerRef}
      />
    </View>
  );
};

export default Content;
