import React, {useState, useRef, useEffect} from 'react';
import {FlatList} from 'react-native';

import {Persons} from '../../../constants/data';

import NavigationBar from '../../common/NavigationBar';
import Avatars, {AVATAR_WIDTH, SEPARATOR_WIDTH} from './Avatars';
import Content from './Content';

const People = () => {
  const [activeAvatarIndex, setActiveAvatarIndex] = useState(0);

  const refAvatars = useRef<FlatList>(null);
  const refContent = useRef<FlatList>(null);

  const handleClick = (index: number) => {
    setActiveAvatarIndex(index);
  };

  useEffect(() => {
    // scroll avatars
    const offset =
      activeAvatarIndex * AVATAR_WIDTH + SEPARATOR_WIDTH * activeAvatarIndex;
    refAvatars?.current?.scrollToOffset({animated: true, offset});

    // scroll content
    refContent?.current?.scrollToIndex({
      animated: true,
      index: activeAvatarIndex,
    });
  }, [activeAvatarIndex]);

  return (
    <>
      <NavigationBar title="Contacts" />
      <Avatars
        activeAvatarIndex={activeAvatarIndex}
        onChangeActiveAvatarIndex={setActiveAvatarIndex}
        innerRef={refAvatars}
        onAvatarClick={handleClick}
        people={Persons}
      />
      <Content
        people={Persons}
        onScrollToPage={handleClick}
        innerRef={refContent}
      />
    </>
  );
};

export default People;
