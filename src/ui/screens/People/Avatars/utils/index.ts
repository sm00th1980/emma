/* eslint import/prefer-default-export: off */

export const getIndexByOffsetX = (
  offsetX: number,
  itemsCount: number,
  itemWidth: number,
  separatorWidth: number,
): number => {
  const totalWidth = itemWidth * itemsCount + separatorWidth * (itemsCount - 1);
  if (offsetX >= totalWidth) {
    // return last index
    return itemsCount - 1;
  }

  return Math.floor((offsetX + separatorWidth) / (itemWidth + separatorWidth));
};
