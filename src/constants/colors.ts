export const CLR_GREY_04 = '#cccccc';
export const CLR_GREY_08 = '#8E8E93';
export const CLR_GREY_09 = 'grey';
export const CLR_GREY_17 = '#e6e6e6';
export const CLR_WHITE = '#ffffff';
export const CLR_BLACK = '#000000';
