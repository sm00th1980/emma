import {getIndexByOffsetX} from '..';

test('example 1', () => {
  const offsetX = -1;
  const itemsCount = 3;
  const itemWidth = 60;
  const separatorWidth = 60;

  expect(
    getIndexByOffsetX(offsetX, itemsCount, itemWidth, separatorWidth),
  ).toBe(0);
});

test('example 2', () => {
  const offsetX = 0;
  const itemsCount = 3;
  const itemWidth = 60;
  const separatorWidth = 60;

  expect(
    getIndexByOffsetX(offsetX, itemsCount, itemWidth, separatorWidth),
  ).toBe(0);
});

test('example 3', () => {
  const offsetX = 30;
  const itemsCount = 3;
  const itemWidth = 60;
  const separatorWidth = 60;

  expect(
    getIndexByOffsetX(offsetX, itemsCount, itemWidth, separatorWidth),
  ).toBe(0);
});

test('example 4', () => {
  const offsetX = 60 + 30;
  const itemsCount = 3;
  const itemWidth = 60;
  const separatorWidth = 60;

  expect(
    getIndexByOffsetX(offsetX, itemsCount, itemWidth, separatorWidth),
  ).toBe(1);
});

test('example 5', () => {
  const offsetX = 60 + 60 + 30;
  const itemsCount = 3;
  const itemWidth = 60;
  const separatorWidth = 60;

  expect(
    getIndexByOffsetX(offsetX, itemsCount, itemWidth, separatorWidth),
  ).toBe(1);
});

test('example 6', () => {
  const offsetX = 60 + 60 + 60 + 30;
  const itemsCount = 3;
  const itemWidth = 60;
  const separatorWidth = 60;

  expect(
    getIndexByOffsetX(offsetX, itemsCount, itemWidth, separatorWidth),
  ).toBe(2);
});

test('example 7', () => {
  const offsetX = 60 + 60 + 60 + 60 + 60 + 30;
  const itemsCount = 3;
  const itemWidth = 60;
  const separatorWidth = 60;

  expect(
    getIndexByOffsetX(offsetX, itemsCount, itemWidth, separatorWidth),
  ).toBe(2);
});
