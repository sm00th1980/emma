import React from 'react';
import {View, StyleSheet, Text} from 'react-native';

import {CLR_BLACK, CLR_GREY_17, CLR_GREY_04} from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    backgroundColor: CLR_GREY_17,
    height: 64,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: CLR_GREY_04,
    borderBottomWidth: 1,
  },
  text: {
    fontSize: 18,
    color: CLR_BLACK,
  },
});

type Props = {
  title: string;
};

const NavigationBar = ({title}: Props) => (
  <View style={styles.container}>
    <Text style={styles.text}>{title}</Text>
  </View>
);

export default NavigationBar;
