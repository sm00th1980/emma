import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  ImageSourcePropType,
  TouchableOpacity,
} from 'react-native';

const {width: TOTAL_WIDTH} = Dimensions.get('window');

const styles = (
  avatarWidth: number,
  separatorWidth: number,
  isFirst: boolean,
  isLast: boolean,
  isActive: boolean,
) => {
  const MARGIN = TOTAL_WIDTH / 2.0 - avatarWidth / 2.0;

  return StyleSheet.create({
    container: {
      marginLeft: isFirst ? MARGIN : undefined,
      justifyContent: 'center',
      width: avatarWidth,
      marginRight: isLast ? MARGIN : undefined,
    },
    avatar: {
      height: avatarWidth,
      width: avatarWidth,
      borderWidth: isActive ? 3 : 1,
      borderColor: isActive ? 'magenta' : undefined,
      borderRadius: avatarWidth,
    },
    separator: {
      width: separatorWidth,
    },
  });
};

type Props = {
  index: number;
  avatarWidth: number;
  separatorWidth: number;
  isFirst: boolean;
  isLast: boolean;
  isActive: boolean;
  src: ImageSourcePropType;
  onClick: (index: number) => void;
};

const Avatar = ({
  avatarWidth,
  separatorWidth,
  isFirst,
  isLast,
  isActive,
  src,
  index,
  onClick,
}: Props) => (
  <>
    <TouchableOpacity
      onPress={() => onClick(index)}
      style={
        styles(avatarWidth, separatorWidth, isFirst, isLast, isActive).container
      }>
      <Image
        style={
          styles(avatarWidth, separatorWidth, isFirst, isLast, isActive).avatar
        }
        source={src}
      />
    </TouchableOpacity>
    {!isLast && (
      <View
        style={
          styles(avatarWidth, separatorWidth, isFirst, isLast, isActive)
            .separator
        }
      />
    )}
  </>
);

export default Avatar;
